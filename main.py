# MicroPython
# Ann 23/02/2020

from machine import Pin, ADC, PWM
from time import sleep
try:
    import usocket as socket
except:
    import socket

''' 
Important things to remember on ESP32

A2 and A3 and A4 are INPUT only
! ADC#2 input doesn't work when WIFI is on, so you need to pick the pins carefully
set attenuator: Full range: 3.3v at 11DB , otherwise the range is 1V and you screw up the board !!
'''


# input
LDR = ADC(Pin(34))        #A2, uses ADC#1 (! ADC#2 doesn't work when WIFI is on)
LDR.atten(ADC.ATTN_11DB)  #Full range: 3.3v (! do this standard range i 1V !)
pot = ADC(Pin(39))        #A3, uses ADC#1
pot.atten(ADC.ATTN_11DB)

# output
LED = PWM(Pin(4), freq=1000) #A4

# connection
# s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
addr_info = socket.getaddrinfo("towel.blinkenlights.nl", 23)
addr = addr_info[0][-1]
s = socket.socket()
s.connect(addr)

# functions
def mapFromInputToPWM(val):
  # analogInput is 12-bit(4095)
  # PWM output is 10-bit (1023)
  return int((val * 1023)/ 4095 )

def mapFromInputToNormalizedFloat(val):
  # analogInput is 12-bit(4096)
  # normalized float is between 0 and 1
  return float(val / 4095)

def mapFromInputToPercent(val):
  # analogInput is 12-bit(4096)
  # percent on 100
  return int((val * 100) / 4095)

# MAIN LOOP

while True:
    # connection
    data = s.recv(500)

    # reading pins
    LDR_value = LDR.read()
    pot_value = pot.read()
    normFloat = mapFromInputToNormalizedFloat(pot_value)
    percntLED = mapFromInputToPercent(pot_value)

    print("LDR: {}".format(LDR_value))
    print("pot: {}".format(pot_value))
    print("normalized float: {}".format(normFloat)
    print("percent LED: {}".format(percntLED)

    LED.duty(mapFromInputToPWM(pot_value))